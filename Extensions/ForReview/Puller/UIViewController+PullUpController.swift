//
//  UIViewController+SBFPullUpController.swift
//  SBFUIKit
//
//  Created by peter.lavskiy on 14/03/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

private var AssociatedObjectKey: UInt8 = 0

// Нужен ли нам этот код для пуллера вообще?
@objc public extension UIViewController {
    
    @objc var backGroundShadowView: UIView? {
        get {
            return objc_getAssociatedObject(UIView.self, &AssociatedObjectKey) as? UIView
        }
        set {
            objc_setAssociatedObject(UIView.self, &AssociatedObjectKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    private var shadowView: UIView {
        
        var bottomInset:CGFloat = 0
        
        if #available(iOS 11.0, *) {
            bottomInset = view.safeAreaInsets.bottom
        } else {
            bottomInset = 0
        }
        
        let size = CGSize(width: view.frame.size.width, height: view.frame.size.height - bottomInset)
        
        let frame = CGRect(origin: view.frame.origin, size: size)
        
        let shadowView = UIView(frame: frame)
        
        shadowView.backgroundColor = .clear
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        
        rootController?.view.addSubview(shadowView)
        
        return shadowView
        
    }
    
    // Данный var является временным решением,
    // так как у нас сейчас в каждом viper модуле идет обращение к window.rootController
    // Как только все модули будут использовать в router собственный viewController для показа пуллера, данный var можно удалить
    private var rootController: UIViewController? {
        
        if let tabBarController = self.tabBarController {
            return tabBarController
        } 
        
        switch self {
        case is UITabBarController:
            let root = self as? UITabBarController
            return root
        case is UINavigationController:
            return self as? UINavigationController
        default:
            return self
        }
    }
    
    @objc func add(pullUpController: SBFPullUpController, animated isAnimated: Bool, completion: (() -> Void)? = nil) {
        
        guard let rootController = rootController else { return }
        
        let recognizer = (rootController as? UINavigationController)?.interactivePopGestureRecognizer
        pullUpController.wasInteractivePopGestureEnabled = recognizer?.isEnabled ?? false
        recognizer?.isEnabled = false
        
        if let existingPullUpcontroller = (rootController.children.first { $0 is SBFPullUpController }) as? SBFPullUpController {
            remove(pullUp: existingPullUpcontroller, animated: isAnimated, completion: { [weak self] in
                self?.add(in: rootController, pullUpController: pullUpController, animated: isAnimated, completion: completion)
            })
        } else {
            add(in: rootController, pullUpController: pullUpController, animated: isAnimated, completion: completion)
        }
    
    }
    
    private func add(in rootController: UIViewController, pullUpController: SBFPullUpController, animated isAnimated: Bool, completion: (() -> Void)? = nil) {
        rootController.backGroundShadowView = shadowView
        rootController.addChild(pullUpController)
        
        pullUpController.view.translatesAutoresizingMaskIntoConstraints = false
        pullUpController.view.frame = CGRect(x: pullUpController.view.frame.origin.x,
                                             y: rootController.view.bounds.height,
                                             width: pullUpController.view.frame.size.width,
                                             height: pullUpController.view.frame.size.height)
        rootController.view.addSubview(pullUpController.view)
        pullUpController.setupPanGestureRecognizer()
        pullUpController.setupTapGestureRecognizer()
        pullUpController.setupConstraints()
        pullUpController.setPortraitConstraints(rootController.view.frame.size)
        pullUpController.view.layoutIfNeeded()
        
        if  pullUpController.pullUpControllerPreviewOffset == 0 {
            
            pullUpController.pullUpControllerMoveToVisiblePoint(pullUpController.pullUpControllerPreferredSize.height,
                                                                animated: isAnimated, completion: nil)
            if pullUpController.onOpenPuller != nil {
                pullUpController.onOpenPuller(isAnimated)
            }
            
        } else {
            
            if isAnimated {
                UIView.animate(withDuration: 0.3, animations: {
                    rootController.backgroundView()?.backgroundColor = pullUpController.blackBackgroundColor
                    rootController.view.layoutIfNeeded()
                })
                
            } else {
                rootController.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func remove(pullUp controller: SBFPullUpController, animated isAnimated: Bool, completion: (() -> Void)? = nil) {

        if controller.wasInteractivePopGestureEnabled {
            (rootController as? UINavigationController)?.interactivePopGestureRecognizer?.isEnabled = true
        }
        controller.shouldHide = true
        controller.pullUpControllerMoveToVisiblePoint(0, animated: isAnimated, completion: {
            controller.removeGestureRecognizers()
            controller.parent?.backGroundShadowView?.removeFromSuperview()
            controller.parent?.backGroundShadowView = nil
            controller.willMove(toParent: nil)
            controller.view.removeFromSuperview()
            controller.removeFromParent()
            completion?()
        })
        controller.viewController?.view.endEditing(true)
    }

}
