//
//  UIColor+Extensions.swift
//  SBFUIKit
//
//  Created by Sergey Zapuhlyak on 17/12/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

import Foundation

public extension UIColor {
    // Нужен ли нам инициализатор для цвета если все цвета у нас перечислены в Colors?
    convenience init(hex: Int, alpha: CGFloat = 1) {
        let r = CGFloat((hex & 0xFF0000) >> 16) / 255
        let g = CGFloat((hex & 0xFF00) >> 8) / 255
        let b = CGFloat((hex & 0xFF)) / 255
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    func adjust(by percent: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: max(min(red + percent/100, 1.0),0),
                           green: max(min(green + percent/100, 1.0),0),
                           blue: max(min(blue + percent/100, 1.0),0),
                           alpha: alpha)
        } else {
            return self
        }
    }
}
