//
//  UITextView+InputFormatter.swift
//  SBBO
//
//  Created by Tatiana Blagoobrazova on 05/06/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension UITextView {
    
    func replaceText(pattern: TextEditFormat, range: NSRange, replacement: String?) -> Bool {
        
        let mask: String
        let maskSymbol: Character
        let regex: String?
        
        switch pattern {
        case .cardNumber:
            mask = Constants.Mask.accountNumber
            maskSymbol = Constants.Mask.maskSymbol
            regex = Constants.Regex.cardNumber
        }
        
        return replace(range: range,
                       replacement: replacement,
                       regex: regex,
                       mask: mask,
                       maskSymbol: maskSymbol)
        
    }
    
    func replace(range: NSRange, replacement: String?, regex: String?, mask: String, maskSymbol: Character) -> Bool {
        
        let textView = self
        var position: Int = 0
        let replacement = replacement ?? ""
        
        let text = InputFormatter.replace(text: textView.text,
                                          range: range,
                                          replacement: replacement,
                                          position: &position,
                                          regex: regex,
                                          mask: mask,
                                          maskSymbol: maskSymbol)
        
        if let text = text, text.count <= mask.count {
            textView.text = text
            
            if replacement.count > 0 {
                DispatchQueue.main.async {
                    let safePosition = max(min(position, textView.text.count), 0)
                    textView.selectedRange = NSRange(location: safePosition, length: 0)
                }
            } else {
                let safePosition = max(min(position, textView.text.count), 0)
                textView.selectedRange = NSRange(location: safePosition, length: 0)
            }
            
            return true
        } else { // не сработало регулярное выражение
            return false
        }
    }
}
