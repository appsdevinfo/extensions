//
//  NSDecimalNumber+DecimalPart.swift
//  SBBO
//
//  Created by Dmitry Melnikov on 18/06/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension NSDecimalNumber {
    
    /// Трансформация числа в вид, подходящи для отображения в полях сумма или количество
    ///
    /// - Parameters:
    ///   - count: количество цифр после запятой
    ///   - separator: разделитель между целой и дробной частями. По умолчанию ","
    func make(decimalPart count: Int, separator: String = ",") -> String {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = count
        formatter.minimumIntegerDigits = 1
        formatter.decimalSeparator = separator
        return formatter.string(from: self) ?? ""
    }
    
}
