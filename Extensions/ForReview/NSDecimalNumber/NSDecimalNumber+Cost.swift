//
//  NSDecimalNumber+Cost.swift
//  SBBO
//
//  Created by Dmitry Melnikov on 20/06/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension NSDecimalNumber {
    
    /// Преобразует NSDecimalNumber в строку вида "XX,XX Y", где XX,XX - число, а Y - символ валюты
    ///
    /// - Parameters:
    ///   - currency: iso4217 код валюты. Если передать nil вернет "₽"
    ///   - separator: по умолчанию ","
    func make(currency: String?, separator: String = ",") -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = " "
        numberFormatter.groupingSize = 3
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.decimalSeparator = separator
        let isoCode: String = currency ?? "643"
        let symbol = Currency(code: isoCode).sign
        return "\(numberFormatter.string(from: self) ?? "") \(symbol)"
    }
}
