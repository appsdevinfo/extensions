//
//  Decimal+Round.swift
//  SBBO
//
//  Created by Tatiana Blagoobrazova on 19/03/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension Decimal {
    
    public func isLess(_ value: Decimal) -> Bool {
        return (self as NSDecimalNumber).compare(value as NSDecimalNumber) == .orderedAscending
    }
    public func isGreater(_ value: Decimal) -> Bool {
        return (self as NSDecimalNumber).compare(value as NSDecimalNumber) == .orderedDescending
    }
    public func isLessOrEqual(_ value: Decimal) -> Bool {
        return (self as NSDecimalNumber).compare(value as NSDecimalNumber) != .orderedDescending
    }
    public func isGreaterOrEqual(_ value: Decimal) -> Bool {
        return (self as NSDecimalNumber).compare(value as NSDecimalNumber) != .orderedAscending
    }
    public func isEqual(_ value: Decimal) -> Bool {
        return (self as NSDecimalNumber).compare(value as NSDecimalNumber) != .orderedSame
    }
    
    public func round() -> Decimal {
       let behavior = NSDecimalNumberHandler(roundingMode: .plain,
                                             scale: 2,
                                             raiseOnExactness: false,
                                             raiseOnOverflow: false,
                                             raiseOnUnderflow: false,
                                             raiseOnDivideByZero: false)
        
        return (self as NSDecimalNumber).rounding(accordingToBehavior: behavior) as Decimal
    }

    public func formatString() -> String {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.decimalSeparator = ","
        return numberFormatter.string(from: self as NSDecimalNumber) ?? ""
        
    }
    
    public func formatRurSum() -> String {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = " "
        numberFormatter.groupingSize = 3
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.decimalSeparator = ","

        if let amount = numberFormatter.string(from: self as NSDecimalNumber),
            let currencyCode = SBTSharedHelper.sharedInstance()?.getCurrencySymbol("RUR") {
            return "\(amount) \(currencyCode)"
        }
  
        return ""
    }

}
