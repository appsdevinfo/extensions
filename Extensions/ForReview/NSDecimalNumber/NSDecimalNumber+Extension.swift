//
//  NSDecimalNumber+Compare.swift
//  SBBO
//
//  Created by Tatiana Blagoobrazova on 19/03/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension NSDecimalNumber {
    
    public func isLess(_ value: NSDecimalNumber) -> Bool {
        return self.compare(value) == .orderedAscending
    }
    public func isGreater(_ value: NSDecimalNumber) -> Bool {
        return self.compare(value) == .orderedDescending
    }
    public func isLessOrEqual(_ value: NSDecimalNumber) -> Bool {
        return self.compare(value) != .orderedDescending
    }
    public func isGreaterOrEqual(_ value: NSDecimalNumber) -> Bool {
        return self.compare(value) != .orderedAscending
    }
    public func isEqual(_ value: NSDecimalNumber) -> Bool {
        return self.compare(value) != .orderedSame
    }
    
    public func round() -> Decimal {
       let behavior = NSDecimalNumberHandler(roundingMode: .plain,
                                             scale: 2,
                                             raiseOnExactness: false,
                                             raiseOnOverflow: false,
                                             raiseOnUnderflow: false,
                                             raiseOnDivideByZero: false)
        
        return self.rounding(accordingToBehavior: behavior) as Decimal
    }

}
