//
//  String+CurrencyInitialization.swift
//  SBBO
//
//  Created by Sergey Zapuhlyak on 11/03/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

public extension String {
    
    init?(decimal value: Decimal, currency: Currency = .ruble) {
        let formatter = NumberFormatter()
        formatter.generatesDecimalNumbers = true
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.usesGroupingSeparator = true
        formatter.decimalSeparator = ","
        formatter.groupingSeparator = " "
        formatter.groupingSize = 3
        formatter.zeroSymbol = "0,00"
        guard let string = formatter.string(from: value as NSDecimalNumber) else { return nil }
        let sign = currency.sign
        let result = currency.isSignPrefix ? sign + " " + string : string + " " + sign
        self = result.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var numberFormat: String {
        return "№ " + self
    }
}
