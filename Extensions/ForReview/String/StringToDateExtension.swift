//
//  StringToDateExtension.swift
//  SBBO
//
//  Created by Imal on 31/01/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import UIKit

// MARK: - вместо методов лучше использовать расширения SwifterSwift в Date+Extensions
extension String {
    
    func shortStringFormatToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: self)
    }
    
    public func fullStringFormatToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "UTC")!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.date(from: self)
    }

    public var deviceUniqueId: String {
        return (self + "0000").replacingOccurrences(of: "-", with: "0")
    }
}

extension Date {
    
    func shortDateFormatToString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    func fullDateFormatToString() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    
}
