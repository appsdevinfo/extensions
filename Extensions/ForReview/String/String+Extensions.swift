//
//  String+Extensions.swift
//  SBFUIKit
//
//  Created by Sergey Zapuhlyak on 18/12/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

import Foundation

public extension String {
    subscript(short: Int) -> Character {
        get {
            return self[index(startIndex, offsetBy: short)]
        }
    }
    
    subscript(start: Int, lenght: Int) -> Substring {
        get {
            let start = index(startIndex, offsetBy: start)
            let end = index(start, offsetBy: lenght)
            return self[start..<end]
        }
    }
}

public extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
    
    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    func replacingFirstOccurrenceOfString(target: String,
                                          withString replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        return self
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self,
                                 tableName: nil,
                                 bundle: Bundle.sbfuiKitResources(),
                                 value: self,
                                 comment: "")
    }
}

public extension String {
    func check(with regularPattern: String) -> Bool {
        if self.isEmpty {
            return true
        }
        
        guard let regularExpression = try? NSRegularExpression(pattern: regularPattern, options: .caseInsensitive)  else {
            return false
        }
        let stringRange = NSRange(location: 0, length: self.count)
        
        let matches = regularExpression.matches(in: self,
                                                options: [],
                                                range: stringRange)
        if matches.isEmpty {
            return false
        }
        
        let matchesResult = matches[0]
        let resultRange = matchesResult.range
        if resultRange.location == stringRange.location
            && resultRange.length == stringRange.length {
            return true
        }
        return false
    }
}
