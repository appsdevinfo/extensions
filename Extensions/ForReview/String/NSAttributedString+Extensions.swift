//
//  NSAttributedString+Extensions.swift
//  SBFUIKit
//
//  Created by Daniil Pendikov on 18/04/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import UIKit

extension String {
    
    var attributed: NSAttributedString {
        return NSAttributedString(string: self)
    }
    
    func addingAttributes(_ attributes: [NSAttributedString.Key: Any], to substring: String) -> NSAttributedString {
        let range = (self as NSString).range(of: substring, options: .caseInsensitive)
        if range.location == NSNotFound {
            return self.attributed
        }
        let attributed = NSMutableAttributedString(string: self)
        attributed.addAttributes(attributes, range: range)
        return attributed
    }
    
}
