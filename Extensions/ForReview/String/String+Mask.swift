//
//  String+Mask.swift
//  SBBO
//
//  Created by Tatiana Blagoobrazova on 30/05/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

public extension String {
    
    func applyCardNumberMask() -> String {
        
        return apply(mask: Constants.Mask.accountNumber,
                     maskSymbol: Constants.Mask.maskSymbol)
    }
    
    // применение маски к строке
    // ex: 4274380010706411 + маска #### #### #### #######: 42743 8001 0706 411
    func apply(mask: String, maskSymbol: Character) -> String {
        
        var newtext = [Character]()
        let textArray = Array(self)
        for i in 0..<textArray.count {
            newtext.append(textArray[i])
            while (mask.count > newtext.count + 1) && mask[newtext.count] != maskSymbol {
                newtext.append(mask[newtext.count])
            }
        }
        return String(newtext)
    }
    
    func remove(mask: String, maskSymbol: Character) -> String {
        
        var textArray = Array(self)
        for i in (0..<textArray.count).reversed() {
            if (mask.count > i + 1) && mask[i] != maskSymbol {
                textArray.remove(at: i)
            }
        }
        return String(textArray)
    }
}
