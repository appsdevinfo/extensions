//
//  String+NumberMask.swift
//  SBBO
//
//  Created by Dmitry Melnikov on 24/05/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

public extension String {
    
    /// Метод для группировки строки согласно шаблону
    ///
    ///     let account = ASTAccount().account
    ///     let result = account.makeNumber(with: Constants.Mask.accountNumber,
    ///                                symbol: Constants.Mask.maskSymbol)
    ///     print(result) // "3256 667 5 48542118955"
    ///
    ///
    /// - Parameters:
    ///   - mask: маски перечислены в Constants.Mask.
    ///   - symbol: Constants.Mask.maskSymbol
    /// - Returns: Напирмер: 
    func makeNumber(with mask: String, symbol: Character) -> String {
        var result = self
        mask.enumerated().forEach {
            if $0.element != symbol {
                result.insert($0.element, at: mask.index(mask.startIndex, offsetBy: $0.offset))
            }
        }
        return result
    }
    
    /// Возвращает номер счета без разделителей.
    var accountWithoutMask: String {
        let result = self.filter {
            "0"..."9" ~= $0
        }
        guard self.check(with: Constants.Regex.account) else { return self }
        return result
    }
    
    /// Преобразует номер счета в короткую форму
    ///
    /// - Parameters:
    ///   - cardType: Тип карты
    /// - Returns: "•••• 6772"
    func applyAccountShortForm(with cardType: String? = "") -> String {
        guard !self.isEmpty,
            let type = cardType else { return "" }
        return type.isEmpty ? "•••• \(self.suffix(4))" : "\(type) •••• \(self.suffix(4))"
    }
}
