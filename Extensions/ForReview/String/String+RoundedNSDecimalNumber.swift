//
//  NSDecimalNumber+Rounded.swift
//  SBBO
//
//  Created by Dmitry Melnikov on 19/06/2019.
//  Copyright © 2019 Sberbank. All rights reserved.
//

import Foundation

extension String {
    /// Возвращает NSDecimalNumber, округлив и заменив кастомные разделители дробной части на "."
    var roundedDecimal: NSDecimalNumber? {
        let value = NSDecimalNumber(string: self.replacingFirstOccurrenceOfString(target: ",",
                                                                                  withString: "."))
        let handler = NSDecimalNumberHandler(roundingMode: .plain,
                                             scale: 2,
                                             raiseOnExactness: false,
                                             raiseOnOverflow: false,
                                             raiseOnUnderflow: false,
                                             raiseOnDivideByZero: false)
        return value.rounding(accordingToBehavior: handler)
    }
}
